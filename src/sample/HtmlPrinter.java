package sample;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class HtmlPrinter extends Application {

    @FXML
    private TextField tf;

    @FXML
    private TextArea ta;

    @FXML
    private WebView wv;

    @FXML
    private ProgressIndicator progress;

    private void OpenPage(String url) {
        progress.setVisible(true);
        Runnable r = () -> {
            Socket kkSocket = null;       // vtičnica
            int port = 80;               // vrata

            PrintWriter out = null;      // izhodni tok
            BufferedReader in = null;    // vhodni tok

            // poskus vzpostavitve povezave: če ne uspe, izstop iz programa
            try {
                kkSocket = new Socket(url, port);
                out = new PrintWriter(kkSocket.getOutputStream(), true);
                in = new BufferedReader(new InputStreamReader(kkSocket.getInputStream()));
            } catch (UnknownHostException e) {
                System.err.println("Don't know about host: " + url);
                System.exit(1);
            } catch (IOException e) {
                System.err.println("Couldn't get I/O for the connection to: " + url);
                System.exit(1);
            }

            // povezava ok

            //test string 1
            //String getSS = "GET / HTTP/1.1\n\n";

            //test string 2
            //String hhh = "GET / HTTP/1.1\nHost: www.arnes.si\nUser-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0\nAccept: image/png,image/*;q=0.8,*/*;q=0.5\nAccept-Language: sl,en-gb;q=0.7,en;q=0.3\n";
            // hhh += "Accept-Encoding: gzip, deflate\nDNT: 1\nReferer: http://www.arnes.si/\nConnection: keep-alive\n\n";

            //test string 3  -- poleg metode in protokola, pošljemo zgolj še  host: in user-agent:
            //                  vnos zaključita 2 loma vrstice
            String abc = "GET / HTTP/1.1\nHost: " + url + "\nUser-Agent: Mozilla/5.0\n\n";

            // test string 4
            //String helo="\n\nhelo\n\n";
            out.println(abc);
            String tmp = "";
            String a;
            try {
                StringBuilder tmpBuilder = new StringBuilder();
                while ((a = in.readLine()) != null) {
                    tmpBuilder.append(a).append("\n");
                    System.out.println(a);
                }
                tmp = tmpBuilder.toString();

                out.close();
                in.close();

                kkSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            final String tmp2 = tmp;
            Platform.runLater(() -> {
                ta.setText(tmp2);
                wv.getEngine().loadContent(tmp2);
                progress.setVisible(false);
            });
            //System.out.println(tmp);
        };
        Thread t = new Thread(r);
        t.start();
    }

    @FXML
    public void OpenWebsite() {
        OpenPage(tf.getText());
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("aplikacija.fxml"));
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.setResizable(false);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
